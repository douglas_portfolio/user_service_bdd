FROM python:latest
MAINTAINER Douglas Sandy Bonafé "dsbonafe@gmail.com"
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
COPY . /user_service_bdd
WORKDIR /user_service_bdd
RUN pip install -r requirements.txt
CMD ["behave"]
CMD ["ifconfig"]


