from singleton_decorator import singleton


@singleton
class Environment:
    env_list = []

    class Data:
        key = str()
        value = object()

    def value_from_key(self, key):
        response = None
        if self.env_list is []:
            raise 'Element not found'
        else:
            for data in self.env_list:
                if data.key is key:
                    response = data.value
        return response

    def all_values(self):
        return self.env_list

    def save_on_environment(self, key, value):
        data = self.Data()
        data.key = key
        data.value = value
        if data not in self.env_list:
            self.env_list.append(data)
