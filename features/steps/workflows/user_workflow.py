import requests
from singleton_decorator import singleton
import json
from features.config.environment import Environment


@singleton
class Flow:

    env = Environment()

    def create_user(self, data):
        url = 'http://localhost:8080/add-user-json'
        r = requests.post(url, json=data)
        self.env.save_on_environment('user', data)
        return r

    def retrieve_user(self):
        url = 'http://localhost:8080/users-json'
        r = requests.get(url)
        print(r.status_code)
        response = list(r.json())
        return self.env.value_from_key('user') in response
