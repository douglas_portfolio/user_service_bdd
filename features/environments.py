from yaml import load
from behave import *


@before_all
def before_all(context):
    context.settings = load(open('features/resources/conf.yaml'))
    context.staging_url = context.settings['target']
